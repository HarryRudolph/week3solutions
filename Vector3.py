import math

class Vector3:
    def __init__ (self, x, y, z):
        self.x = x
        self.y = y
        self.z = z

    def azimuthalAngle(self):
        if self.x == 0 and self.y == 0:
            return 0
        else:
            return math.atan2(self.y, self.x)


    def elevationAngle(self):
        if self.x == 0 and self.y == 0 and self.z == 0:
            return 0
        else:
            return math.atan2((math.sqrt(self.x**2 + self.y**2)), self.z)

    def resultant(self):
        return math.sqrt(self.x**2 + self.y**2 + self.z**2)



if __name__ == "__main__":
    vec = Vector3(1, 1, 1)

    print(math.degrees(vec.azimuthalAngle()))
    print(math.degrees(vec.elevationAngle()))
    print(math.degrees(vec.resultant()))
    
