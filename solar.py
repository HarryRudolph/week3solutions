class SolarPanel:
    def __init__(self, serialNumber, currentPowerProd):
        self.serialNumber = serialNumber
        self.currentPowerProd = currentPowerProd

class SolarArray:
    def __init__(self, x, y, solarPanels):
        self.x = x
        self.y = y
        self.solarPanels = solarPanels

    def totalPower(self):
        total = 0
        for panel in self.solarPanels:
            total += panel.currentPowerProd
        return total

if __name__ == "__main__":
    solarList = [SolarPanel("123", 1.0),
                 SolarPanel("456", 2.0),
                 SolarPanel("789", 3.0)]
    
    sa = SolarArray(1, 1, solarList)

    print(sa.totalPower())
    
