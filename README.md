# Week 3 Exercises

2. Create a class to represent a three-vector. This class should have a constructor that accepts three arguments, which are the x, y, and z components of the vector. The class should have three public data members that are used to store the x, y and z components separately. Create three member functions to:
 - Return the azimuthal angle (φ) between the x and y components.
 - Return the elevation angle (θ) between the x-y plan and the z axis.
 - Return the resultant (r) of the vector.

4. Edit minimise.py and add a function that:
 - Picks three random x values within the range −2 < x < 5.
 - Selects the two lowest values of the three calculated.
 - Selects a new third value at a random x point between the two remaining values.
 - Continues until the difference between the three points or two remaining values is less than 0.001. The difference should be computed as the difference between the points along the x-axis, which is illustrated in Figure 3.
 - Returns the value of x for the resulting minima.
